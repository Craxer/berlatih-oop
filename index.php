<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index PHP</title>
</head>
<body>
    <?php 
    require('animal.php');
    require('Ape.php');
    require('Frog.php');

    $sheep = new Animal("shaun");
    
    echo "Nama Binatang : " .$sheep-> name. "<br>";
    echo "Jumlah Kaki : " .$sheep -> legs. "<br>";
    echo "Apakah berdarah dingin (True/False)?  " .$sheep -> cold_blooded. "<br><br>";

    $object1 = new Ape("kera sakti");
    echo "Nama Binatang : " .$object1 -> name. "<br>";
    echo " Jumlah Kaki : " .$object1 -> legs. "<br>";
    echo "Apakah berdarah dingin? " .$object1 -> cold_blooded. "<br>";
    $object1 -> yell();
    echo "<br><br>";

    $object2 = new Frog("buduk");
    echo "Nama Binatang : " .$object2 -> name. "<br>";
    echo " Jumlah Kaki : " .$object2 -> legs. "<br>";
    echo "Apakah  berdarah dingin? " .$object2 -> cold_blooded. "<br>";
    echo $object2 -> jump();
?>
</body>
</html>